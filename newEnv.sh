read -r -p 'This script will create a Python Environment in the folder it is run from. Are you sure you want to continue? [Y/n]'
response=${response,,} # toLower
    if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
        # do dangerous stuff
        echo '#################~ Name your amazing project ~#################'
        read -p 'Project Name: ' projectname
        
        mkdir $projectname
        cd $projectname
        # Create new environment inside new folder
        mkdir project-files
        python -m venv . --without-pip
        #copy get-pip.py to new environment
        # or download latest pip from https://bootstrap.pypa.io/get-pip.py
        echo '#################~ Downloading latest pip ~#################'
        #This Program needs get-pip.py file to work and the file needs to be in installation directory of python
        wget https://bootstrap.pypa.io/get-pip.py
        echo '#################~ Initializing pip ~#################'
        ./bin/python3.9 get-pip.py
        echo '#################~ Run source activate from inside the Penv/bin folder ~#################'
        # echo '#################~ (source ./bin/activate vanuit de env folder root) ~#################'
        # source ./bin/activate
    #TODO:make else statement to cancel
    fi