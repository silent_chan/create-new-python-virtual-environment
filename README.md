# Create New Python Virtual Environment

## HOW TO USE:

- Run the script where you want to create a new python environment.
- It will ask you for a project name, a folder will be created with that name.
- Newest version of pip will be downloaded and installed.
- Run `source ./bin/activate` from inside the project root folder.


